# Python Echo Socket

This project implements TCP and UDP socket server and client.

## Usage

Start the server:

```bash
$ python3 echo-server.py
```

Start the client:

```bash
$ python3 echo-client.py
```

Show help:

```bash
$ python3 echo-server.py -h
```

Also for client

```bash
$ python3 echo-client.py -h
```


## Aditional Options

- `--ip`: defines server IP
- `--port`: defines server Port
- `--type`: can be `UDP` or `TCP`
- `--message`: defines client socket payload

## 