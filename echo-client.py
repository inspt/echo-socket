#!/usr/bin/env python3

import argparse
import socket

from constants import ENCODING
from helpers import validate_type, validate_ip, validate_port, parser


def start_tcp_client(server_address, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(server_address)
        s.sendall(message)
        data = s.recv(args.buffer_size)
        print(f"Received {data} from {host}")


def start_udp_client(server_address, message):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.sendto(message, server_address)
        data, addr = s.recvfrom(args.buffer_size)
        print(f'Message from {addr}: {data}')


if __name__ == "__main__":
    parser.description='Echo Socket Client'
    parser.add_argument('-m', '--message',
        help=f"Message on socket payload",
        default='Hello World!'
        )
    args = parser.parse_args()

    host = validate_ip(host=args.ip)
    port = validate_port(port=args.port)
    socket_type = validate_type(socket_type=args.type)

    print(f'Starting Echo {args.type.upper()} Socket Client to {host}:{port}')
    server_address = (host, port)
    message = bytes(args.message, ENCODING)
    try:
        if socket_type == 'TCP':
            start_tcp_client(server_address, message)
        elif socket_type == 'UDP':
            start_udp_client(server_address, message)
        else:
            raise NotImplementedError
    except ConnectionRefusedError:
        print('ERROR: Server not detected!')
        print(f'Did you start the server on {host}:{port}?')
        exit(4)
    except OSError as e:
        print(e)
        exit(e.errno)
    except KeyboardInterrupt:
        print('\n====== <Ctrl>+C detected ======')
        print('Exiting!')
        exit(0)
