#!/usr/bin/env python3

import argparse
import socket

from constants import BUFFER, HOST, PORT
from helpers import validate_ip, validate_port, validate_type, parser


def start_tcp_server(server_address):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(server_address)
        s.listen()
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(args.buffer_size)
                if not data:
                    break
                conn.sendall(data)
                print(f"Sending {data} to {addr}")


def start_udp_server(server_address):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind(server_address)
        while True:
            data, addr = s.recvfrom(args.buffer_size)
            print(f'Message from {addr}: {data}')
            if not data:
                break
            s.sendto(data, addr)
            print(f"Sending {data} to {addr}")


if __name__ == "__main__":
    args = parser.parse_args()

    host = validate_ip(host=args.ip)
    port = validate_port(port=args.port)
    socket_type = validate_type(socket_type=args.type)

    print(f'Starting Echo {args.type.upper()} Socket Server on {host}:{port}')
    server_address = (host, port)
    while True:
        try:
            if socket_type == 'TCP':
                start_tcp_server(server_address)
            elif socket_type == 'UDP':
                start_udp_server(server_address)
            else:
                raise NotImplementedError
        except OSError as e:
            print(e)
            exit(e.errno)
        except KeyboardInterrupt:
            print('\n====== <Ctrl>+C detected ======')
            print('Exiting!')
            exit(0)
