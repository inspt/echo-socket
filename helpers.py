import argparse
import ipaddress

from constants import HOST, PORT, BUFFER


parser = argparse.ArgumentParser(description='Echo Socket Server')
parser.add_argument('-i', '--ip',
    help=f'Specify a server IP socket to bind. Default to {HOST}',
    default=f"{HOST}",
    )
parser.add_argument('-p', '--port',
    help=f'Specify a server PORT socket to bind. Default to {PORT}',
    default=f"{PORT}",
    )
parser.add_argument('-b', '--buffer-size',
    help=f'Specify a server PORT socket to bind. Default to {BUFFER}',
    default=BUFFER,
    )
parser.add_argument('-t', '--type',
    help=f'Socket Type: TCP or UDP. Default to TCP',
    default='tcp',
    )


def validate_ip(host, error_code=1):
    host = '0.0.0.0' if host == '*' else host
    try:
        ipaddress.ip_address(host)
    except ValueError:
        print(f"{host} doesn't look like a valid IP")
        exit(error_code)
    return host


def validate_port(port, min_port=1023, max_port=65535, error_code=2):
    try:
         port = int(port)
         if port < min_port or port > max_port:
            raise ValueError(f"{min_port} > {port} < {max_port}")
    except ValueError:
        print(f"{port} doesn't look like a valid PORT number")
        exit(error_code)

    return port


def validate_type(socket_type, error_code=3):
    socket_type = socket_type.upper()
    try:
        if socket_type == 'TCP' or socket_type == 'UDP':
            return socket_type
        raise ValueError('Socket Type must be TCP or UDP')
    except ValueError as e:
        print(e)
        exit(error_code)